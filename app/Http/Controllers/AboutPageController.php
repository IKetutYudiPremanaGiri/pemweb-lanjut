<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\index;
class AboutPageController extends Controller
{
    public function about()
    {
        return view('about');
    }
}