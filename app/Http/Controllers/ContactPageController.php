<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\index;
class ContactPageController extends Controller
{
    public function contact()
    {
        return view('contact');
    }
}