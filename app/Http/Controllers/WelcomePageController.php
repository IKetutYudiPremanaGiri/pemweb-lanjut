<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\index;
class WelcomePageController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }
}